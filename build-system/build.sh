#!/bin/bash
echo "input bytewalk version without the 'v'"
read -p "bytewalk version: " bytewalk_version
scriptdir=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
sudo docker build --build-arg bytewalk_version=$bytewalk_version -t bytewalk-builder bytewalk-builder
sudo rm -rf $scriptdir/artifacts/*
sudo docker run -i -v $scriptdir/artifacts:/artifacts -t bytewalk-builder /builds/export_artifacts.sh
